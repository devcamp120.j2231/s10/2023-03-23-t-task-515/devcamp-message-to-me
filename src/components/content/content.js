import { Component } from "react";
import InputContent from "./input/inputContent";
import OutputContent from "./output/outputContent";

class Content extends Component {
    render() {
        return (
            <>
                <InputContent/>
                <OutputContent/>
            </>
        )
    }
}

export default Content