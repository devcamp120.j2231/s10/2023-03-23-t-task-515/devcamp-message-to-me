import { Component } from "react";
import ImageHeader from "./image/imageHeader";
import TextHeader from "./text/textHeader";

class Header extends Component {
    render() {
        return (
            <>
                <TextHeader/>
                <ImageHeader/>
            </>
        )
    }
}

export default Header